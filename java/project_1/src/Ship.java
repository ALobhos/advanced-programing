
public class Ship {

	
	private int id;
	private String body_type;
	//private int position;
	private Fuel_tank fuel;
	private Wing wings;
	private Wheel wheels;
	private Odometer odometer;
	//private Pista track;
	
	Ship(int id, String body_type, String fuel){
		
		this.id = id;
		this.body_type = body_type;
		this.wings = new Wing();
		this.wheels = new Wheel();
		this.fuel = new Fuel_tank(fuel);
		this.odometer = new Odometer();

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBody_type() {
		return body_type;
	}

	public void setBody_type(String body_type) {
		this.body_type = body_type;
	}

	public Fuel_tank getFuel() {
		return fuel;
	}

	public void setFuel(Fuel_tank fuel) {
		this.fuel = fuel;
	}

	public Wing getWings() {
		return wings;
	}

	public void setWings(Wing wings) {
		this.wings = wings;
	}

	public Wheel getWheels() {
		return wheels;
	}

	public void setWheels(Wheel wheels) {
		this.wheels = wheels;
	}

	public Odometer getOdometer() {
		return odometer;
	}

	public void setOdometer(Odometer odometer) {
		this.odometer = odometer;
	}

	
	
}

package ejercicio1;

public class Persona {
	
	private String nombre;
	private Mano mano_derecha;
	private Mano mano_izquierda;
	
	Persona(String nombre){
		this.mano_izquierda = new Mano();
		this.mano_izquierda = new Mano();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Mano getMano_derecha() {
		return mano_derecha;
	}

	public void setMano_derecha(Mano mano_derecha) {
		this.mano_derecha = mano_derecha;
	}

	public Mano getMano_izquierda() {
		return mano_izquierda;
	}

	public void setMano_izquierda(Mano mano_izquierda) {
		this.mano_izquierda = mano_izquierda;
	}
	
}

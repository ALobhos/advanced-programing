package ejercicio1;

public class Mano {
	
	private String estado;
	private String jugada;
	
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getJugada() {
		return jugada;
	}
	public void setJugada(String jugada) {
		this.jugada = jugada;
	}

}

package ejercicio1;
import java.util.Scanner;


public class Main {
	
	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		

		Persona jugador_1 = new Persona("Jugador 1");
		Persona jugador_2 = new Persona("Jugador 2");
		
		System.out.println("¿Que modalidad desean jugar?");
		System.out.println("1.- Cachipun Simple");
		System.out.println("2.- Cachipun Doble");
		
		int opcion = scan.nextInt();
		String ganador = "nadie";
		
		if(opcion == 1) {
			System.out.println("Se seleccionó la opcion de cachipun simple");
			ganador = juego(opcion, jugador_1, jugador_2);
		}
		else {
			System.out.println("Se seleccionó la opcion de cachipun doble");
		}
	}
	
	public static String juego(int modalidad, Persona jugador_1, Persona jugador_2) {
		
		
		System.out.println("¿Que jugada desea hacer el primer jugador?");
		System.out.println("(piedra(d), papel(p) o tijeras(t))");
		jugador_1.getMano_derecha().setJugada(play_maker(jugador_1));
		System.out.println("¿Que jugada desea hacer el segundo jugador?");
		System.out.println("(piedra(d), papel(p) o tijeras(t))");
		jugador_2.getMano_derecha().setJugada(play_maker(jugador_2));
		
		
		
		return "hola";
	}
	
	private static String play_maker(Persona jugador) {
		
		String jugada = scan.next();
		
		if (jugada.equals("d") || jugada.equals("p") || jugada.equals("t")) {
			System.out.println("válida");
		}
		else {
			System.out.println("Jugada no válida");
			play_maker(jugador);
		}
		
		return jugada;
	}

}



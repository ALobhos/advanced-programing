BEGIN{
    #Se declara un contador en 1 para ser utilizado como iterador
    count=1
}

#Función que calcula la distancia entre dos átomos

function distance(residue, ligand) {

    #Se calcula la diferencia de distancia xyz y se eleva cada valor al
    #cuadrado
    x = (ligand["X"] - residue["X"]) ^ 2
    y = (ligand["Y"] - residue["Y"]) ^ 2
    z = (ligand["Z"] - residue["Z"]) ^ 2

    #La distancia sera la raiz cuadrada de estos valores sumados (lo que se
    #calcula es un vector que lleve de un átomo al otro, a continuación se
    #calcula el módulo de este, que es su longitud)
    dis = sqrt(x+y+z)

    return dis
}

#Dado que este programa procesa dos archivos, se le pone una condición
#para que comience en el primer archivo
#NR es la linea actual acumulada (todas las lineas de ambos archivos)
#FNR es la linea actual del archivo actual
FNR==NR{

    #Solo se utiliza la fila que comience con "CENTER" (procesado previamente)
    if ($1 == "CENTER"){

        #Se obtienen coordenadas xyz, además del nombre y cadena
        lig_id[count]["name"] = $2
        lig_id[count]["chain"] = $3
        ligand[count]["X"] = $4
        ligand[count]["Y"] = $5
        ligand[count]["Z"] = $6
        count++; #Aquí entra en juego el contador mencionado antes
    }
next
}

#Cuando ya no se cumple la condición anterior, se está procesando el 2do
#archivo, en este caso las coordenadas de los residuos y átomos
{
    #Se crea un id único para cada átomo
    id = $2$3$4
    residue[id]["X"] = $5
    residue[id]["Y"] = $6
    residue[id]["Z"] = $7

}

END{

    #Al final se recorren ambos arreglos de datos obtenidos de ambos
    #archivos, midiendo la distancia entre los centros geométricos de
    #cada ligando y los átomos que forman cada residuo
    for (i in residue){
        for (j in ligand){

            dis = distance(residue[i], ligand[j])

            if (dis <= limit){

                #Se agregan las interacciones encontradas al final del
                #archivo de ligandos, debido a que se utilizará nuevamente
                print "INTER " lig_id[j]["name"] lig_id[j]["chain"] " -> " i
            }
        }
    }
}
